<?php

namespace Database\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20180903090234 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

//        $this->addSql('DROP TABLE hibernate_sequence');
        $this->addSql('ALTER TABLE transactions CHANGE reference reference INT AUTO_INCREMENT NOT NULL, CHANGE account_number account_number VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE balances CHANGE account_number account_number VARCHAR(255) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

//        $this->addSql('CREATE TABLE hibernate_sequence (next_val BIGINT DEFAULT NULL) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE balances CHANGE account_number account_number VARCHAR(10) NOT NULL COLLATE latin1_swedish_ci');
        $this->addSql('ALTER TABLE transactions CHANGE reference reference BIGINT NOT NULL, CHANGE account_number account_number VARCHAR(255) DEFAULT NULL COLLATE latin1_swedish_ci');
    }
}
