<?php
/**
 * Created by PhpStorm.
 * User: austine
 * Date: 03/09/2018
 * Time: 7:40 AM
 */

namespace App\Services\Defaults;


use App\Exceptions\TransferServiceException;
use App\Models\Account;
use App\Models\Transaction;
use App\Repositories\AccountRepository;
use App\Repositories\TransactionRepository;
use App\Services\TransferService;
use Exception;
use http\Exception\InvalidArgumentException;
use Illuminate\Support\Facades\Log;

class DefaultTransferService implements TransferService
{

    /**
     * @var AccountRepository
     */
    protected $accountsRepository;

    /**
     * @var TransactionRepository
     */
    protected $transactionsRepository;

    public function __construct(AccountRepository $accountsRepository, TransactionRepository $transactionsRepository)
    {
        $this->accountsRepository = $accountsRepository;
        $this->transactionsRepository = $transactionsRepository;
    }

    /**
     * @param string $fromAccount The account to debit
     * @param string $toAccount The account to credit
     * @param float $amount The amount to be transferred
     * @return float
     * @throws TransferServiceException
     */
    public function transfer(string $fromAccount, string $toAccount, float $amount)
    {
        if ($fromAccount === $toAccount) {
            throw new TransferServiceException("Cannot transfer to the same account");
        }

        if ($amount < 100) {
            throw new TransferServiceException("Cannot transfer less than #100");
        }

        Log::info("Transferring ".$amount." from ".$fromAccount." to ".$toAccount);

        //Fetch accounts
        $transferringAccount = $this->accountsRepository->findByAccountNumber($fromAccount);
        $receivingAccount = $this->accountsRepository->findByAccountNumber($toAccount);

        Log::info("Pre-Transfer Account Details.");
        Log::info("From Account: \nAccount Number: ".$transferringAccount->getAccountNumber());
        Log::info("Balance: ".$transferringAccount->getBalance());

        Log::info("To Account: \nAccount Number: ".$receivingAccount->getAccountNumber());
        Log::info("Balance: ".$receivingAccount->getBalance());

        //Generate new transaction
        $transaction = $this->validateAccountForTransaction($fromAccount, $toAccount, $amount, $transferringAccount, $receivingAccount);

        Log::info("Performing Transfer...");

        //Update account balances
        $balance = $transferringAccount->getBalance() - $amount;
        $transferringAccount->setBalance($balance);
        $receivingAccount->setBalance($receivingAccount->getBalance() + $amount);

        Log::info("Persisting updated models...");

        //Persist account balances
        $this->accountsRepository->save($transferringAccount);
        $this->accountsRepository->save($receivingAccount);

        //Persist transactions
        $this->transactionsRepository->save($transaction);

        Log::info("Post-Transfer Account Details.");
        Log::info("From Account: \nAccount Number: ".$transferringAccount->getAccountNumber());
        Log::info("Balance: ".$transferringAccount->getBalance());

        Log::info("To Account: \nAccount Number: ".$receivingAccount->getAccountNumber());
        Log::info("Balance: ".$receivingAccount->getBalance());

        return $transferringAccount->getBalance();
    }

    /**
     * @param string $fromAccount
     * @param string $toAccount
     * @param float $amount
     * @param Account $transferringAccount
     * @param Account $receivingAccount
     * @return Transaction
     * @throws TransferServiceException
     * @throws Exception
     */
    private function validateAccountForTransaction(string $fromAccount, string $toAccount, float $amount, Account $transferringAccount, Account $receivingAccount)
    {
        if ($transferringAccount == null) {
            throw new Exception("No account found with account number: " . $fromAccount);
        }

        if ($receivingAccount == null) {
            throw new Exception("No account found with account number: " . $toAccount);
        }

        if ($transferringAccount->getBalance() < $amount) {
            throw new TransferServiceException("Insufficient funds");
        }

        return new Transaction($fromAccount, $amount);
    }
}