<?php
/**
 * Created by PhpStorm.
 * User: austine
 * Date: 02/09/2018
 * Time: 4:15 PM
 */

namespace App\Services\Defaults;


use App\Models\Account;
use App\Repositories\AccountRepository;
use App\Services\AccountsService;
use Illuminate\Support\Facades\Log;

class DefaultAccountsService implements AccountsService
{

    /**
     * @var AccountRepository Repository to obtain account objects.
     */
    protected $accountRepository;

    /**
     * AccountsServiceImpl constructor.
     * @param AccountRepository $accountRepository
     */
    public function __construct(AccountRepository $accountRepository)
    {
        $this->accountRepository = $accountRepository;
    }

    /**
     * Creates a new account.
     *
     * @param Account $account The account to be created
     */
    function create(Account $account)
    {
        $this->accountRepository->save($account);
        // TODO: Implement create() method.
    }

    /**
     * Retrieves all accounts
     */
    function getAccounts()
    {
        Log::info("Getting all accounts...");
        return $this->accountRepository->findAll();
    }

    public function getAccount(string $accountNumber)
    {
        Log::info("Retrieve account info for account: ".$accountNumber);
        return $this->accountRepository->findByAccountNumber($accountNumber);
    }
}