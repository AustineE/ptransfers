<?php
/**
 * Created by PhpStorm.
 * User: austine
 * Date: 03/09/2018
 * Time: 7:37 AM
 */

namespace App\Services;


interface TransferService
{
    /**
     * @param string $fromAccount
     * @param string $toAccount
     * @param float $amount
     * @return double The balance left for the debited account after the transaction.
     */
    public function transfer(string $fromAccount, string $toAccount, float $amount);
}