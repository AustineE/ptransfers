<?php
/**
 * Created by PhpStorm.
 * User: austine
 * Date: 02/09/2018
 * Time: 4:14 PM
 */

namespace App\Services;


use App\Models\Account;

interface AccountsService
{
    public function create(Account $account);

    public function getAccounts();

    public function getAccount(string $accountNumber);
}