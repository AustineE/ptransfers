<?php
/**
 * Created by PhpStorm.
 * User: austine
 * Date: 02/09/2018
 * Time: 11:28 PM
 */

namespace App;


use Symfony\Component\HttpFoundation\JsonResponse;

class ApiResponse extends JsonResponse
{
    public function __construct($data = null, $errors = [], int $status = 200, array $headers = array(), bool $json = false)
    {
        parent::__construct(new ApiResponseModel($data, $status == 200, $errors), $status, $headers, $json);
    }
}


class ApiResponseModel {
    /**
     * @var boolean
     */
    public $success = true;

    /**
     * @var mixed
     */
    public $result;

    /**
     * @var array
     */
    public $errors = [];

    public function __construct($result, $success = true, $errors = [])
    {
        $this->success = $success;
        $this->result = $result;
        $this->errors = $errors;
    }
}