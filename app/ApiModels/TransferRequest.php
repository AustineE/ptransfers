<?php
/**
 * Created by PhpStorm.
 * User: austine
 * Date: 03/09/2018
 * Time: 8:23 AM
 */

namespace App\ApiModels;


class TransferRequest
{
    /**
     * @var string
     */
    private $fromAccount;

    /**
     * @var string
     */
    private $toAccount;

    /**
     * @var double
     */
    private $amount;

    /**
     * @return string
     */
    public function getFromAccount()
    {
        return $this->fromAccount;
    }

    /**
     * @param string $fromAccount
     */
    public function setFromAccount($fromAccount): void
    {
        $this->fromAccount = $fromAccount;
    }

    /**
     * @return string
     */
    public function getToAccount()
    {
        return $this->toAccount;
    }

    /**
     * @param string $toAccount
     */
    public function setToAccount($toAccount): void
    {
        $this->toAccount = $toAccount;
    }

    /**
     * @return double
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param double $amount
     */
    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }


}