<?php

namespace App\Providers;

use App\Repositories\AccountRepository;
use App\Repositories\Defaults\DefaultAccountRepository;
use App\Repositories\Defaults\DefaultTransactionRepository;
use App\Repositories\TransactionRepository;
use App\Services\AccountsService;
use App\Services\Defaults\DefaultAccountsService;
use App\Services\Defaults\DefaultTransferService;
use App\Services\TransferService;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * All of the container bindings that should be registered.
     *
     * @var array
     */
    public $bindings = [
        //Bind Repositories
        AccountRepository::class => DefaultAccountRepository::class,
        TransactionRepository::class => DefaultTransactionRepository::class,

        //Bind Services
        AccountsService::class => DefaultAccountsService::class,
        TransferService::class => DefaultTransferService::class,
    ];


    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
