<?php

namespace App\Exceptions;

use App\ApiResponse;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Log;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception $exception
     * @return ApiResponse
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        if ($exception->getMessage() === "") {
            parent::report($exception);
        } else {
            Log::error("Reporting exception.. \n" . $exception->getTraceAsString());
            return new ApiResponse(null, [$exception->getMessage()], 500);
        }
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response
     */
    public function render($request, Exception $exception)
    {
        Log::error("Rendering exception.. " . $exception->getMessage());

        if ($exception->getMessage() === "") {
            return parent::render($request, $exception);
        }
        return new ApiResponse(null, [$exception->getMessage()], 500);
    }
}
