<?php
/**
 * Created by PhpStorm.
 * User: austine
 * Date: 02/09/2018
 * Time: 9:27 PM
 */

namespace App\Repositories;


use Exception;

class RepositoryException extends Exception
{

    /**
     * RepositoryException constructor.
     * @param string $message
     */
    public function __construct(string $message)
    {
        $this->message = $message;
    }
}