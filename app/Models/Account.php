<?php

namespace App\Models;

use Doctrine\ORM\Mapping AS ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class Account
 * @package App
 * @ORM\Entity
 * @ORM\Table(name="balances")
 */
class Account
{
//    /**
//     * @ORM\Id
//     * @ORM\GeneratedValue
//     * @ORM\Column(type="integer")
//     */
//    protected $id;

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    public $accountNumber;

    /**
     * @ORM\Column(type="float")
     * @var double
     */
    public $balance;

    /**
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * @param string $accountNumber
     */
    public function setAccountNumber($accountNumber): void
    {
        $this->accountNumber = $accountNumber;
    }

    /**
     * @return double
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param double $balance
     */
    public function setBalance($balance): void
    {
        $this->balance = $balance;
    }
}
