<?php
/**
 * Created by PhpStorm.
 * User: austine
 * Date: 02/09/2018
 * Time: 8:21 PM
 */

namespace App\Models;

use Doctrine\ORM\Mapping AS ORM;

/**
 * Class Transaction
 * @package App\Models
 * @ORM\Entity
 * @ORM\Table(name="transactions")
 */
class Transaction
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    public $reference;

    /**
     * @var string
     * @ORM\Column(name="account_number")
     */
    public $accountNumber;

    /**
     * @var double
     * @ORM\Column(type="float")
     */
    public $amount;

    public function __construct($accountNumber, $amount)
    {
        $this->accountNumber = $accountNumber;
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function getReference(): int
    {
        return $this->reference;
    }

    /**
     * @param int $reference
     */
    public function setReference(int $reference): void
    {
        $this->reference = $reference;
    }

    /**
     * @return string
     */
    public function getAccountNumber(): string
    {
        return $this->accountNumber;
    }

    /**
     * @param string $accountNumber
     */
    public function setAccountNumber(string $accountNumber): void
    {
        $this->accountNumber = $accountNumber;
    }


}