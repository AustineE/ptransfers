<?php
/**
 * Created by PhpStorm.
 * User: austine
 * Date: 02/09/2018
 * Time: 5:13 PM
 */

namespace App\Repositories;

use App\Models\Account;
use App\Repositories\Base\Repository;

interface AccountRepository extends Repository
{
    /**
     * @return array
     */
    public function findAll();

    /**
     * @param $accountNumber
     * @return Account
     */
    public function findByAccountNumber($accountNumber);

}