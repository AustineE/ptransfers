<?php
/**
 * Created by PhpStorm.
 * User: austine
 * Date: 02/09/2018
 * Time: 5:16 PM
 */

namespace App\Repositories\Defaults;


use App\Models\Account;
use App\Repositories\AccountRepository;
use App\Repositories\Base\BaseRepository;
use Illuminate\Support\Facades\Log;

class DefaultAccountRepository extends BaseRepository implements AccountRepository
{
    function findByAccountNumber($accountNumber)
    {
        /** @var Account $account */
        $account = ($this->findOneBy(['accountNumber' => $accountNumber]));
        Log::info("Found account: ".$account->getAccountNumber());
        return $account;
    }

//    protected function entity()
//    {
//        return Account::class;
//    }
}