<?php
/**
 * Created by PhpStorm.
 * User: austine
 * Date: 03/09/2018
 * Time: 7:43 AM
 */

namespace App\Repositories\Defaults;


use App\Repositories\Base\BaseRepository;
use App\Repositories\TransactionRepository;

class DefaultTransactionRepository extends BaseRepository implements TransactionRepository
{
    //Nothing to do!
}