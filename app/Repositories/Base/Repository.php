<?php
/**
 * Created by PhpStorm.
 * User: austine
 * Date: 02/09/2018
 * Time: 3:32 PM
 */

namespace App\Repositories\Base;


interface Repository
{
    function find($id);

    function findAll();

    function save($object);
}