<?php
/**
 * Created by PhpStorm.
 * User: austine
 * Date: 02/09/2018
 * Time: 6:05 PM
 */

namespace App\Repositories\Base;


use App\Repositories\RepositoryException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use Illuminate\Support\Facades\Log;

abstract class BaseRepository extends EntityRepository implements Repository
{
    public function __construct(EntityManager $entityManager)
    {
        $metadata = new ClassMetadata($this->getEntityName());
        parent::__construct($entityManager, $metadata);
    }


    /**
     * @param array $data
     * @return mixed
     * @throws RepositoryException
     * @throws \Doctrine\ORM\ORMException
     */
    public function createEntity(array $data)
    {
        $entity = new $this->_entityName;
        foreach ($data as $field => $value) {
            $methodName = 'set' . ucfirst($field);
            if (!method_exists($entity, $methodName)) {
                throw new RepositoryException('Method'.$methodName." does not exist.");
            }
            $entity->$methodName($value);
        }
        $this->save($entity);

        return $entity;
    }


    /**
     * @param $object
     * @throws \Doctrine\ORM\ORMException
     */
    public function save($object)
    {
        Log::info("Persisting object...");
        $this->getEntityManager()->persist($object);
        $this->getEntityManager()->flush();
        Log::info("Successfully updated object.");
    }


    /**
     * Gets the name of the Entity associated with this Repository.
     *
     * @return string The Entity class name for this Repository
     */
    protected function getEntityName(){
        Log::info("Getting Entity Name for ".get_class($this));

        $absoluteRepositoryClass = $this->getRepositoryInterfaceAbsoluteClassName();
        $repositoryClassName = $this->getRepositoryNameFromAbsoluteClassName($absoluteRepositoryClass);

        Log::info("Obtained repository class name as ".$repositoryClassName);

        $entityName = $this->getEntityNameFromRepositoryName($repositoryClassName);

        Log::info("Concatenating...");
        Log::info("Entity Name: ".$entityName);

        $entity = join('\\', ['App\Models', $entityName]);
        Log::info("Obtained entity is ".$entity);

        return $entity;
    }


    /**
     * @return null|mixed
     */
    private function getRepositoryInterfaceAbsoluteClassName() {
        $interfaces = class_implements($this);

        $interfaces = array_filter($interfaces, function (string $interface) {
            return substr($interface, 0, strlen("App")) === "App"
                && (substr($interface, -strlen("\Repository")) !== "\Repository");
        });

        Log::info("Returning first occurrence...");
        foreach ($interfaces as $value) {
            Log::info("Repository implements ".$value);
            return $value;
        }

        Log::info("No Occurrences. Returning null");
        return null;
    }

    /**
     * @param $absoluteClassName string The absolute Entity Interface Name
     * @return mixed
     */
    private function getRepositoryNameFromAbsoluteClassName($absoluteClassName)
    {
        $parts = explode("\\", $absoluteClassName);
        return $parts[count($parts) - 1];
    }

    /**
     * @param $entityClassName
     * @return array[]|false|string[]
     */
    private function getEntityNameFromRepositoryName($entityClassName)
    {
        $entityNameIndex = 1;
        return preg_split('/(?=[A-Z])/', $entityClassName)[$entityNameIndex];
    }
}