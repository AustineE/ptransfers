<?php

namespace App\Http\Controllers;

use App\ApiResponse;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param $data mixed The result of the response.
     * @return ApiResponse The response body
     */
    protected function success($data) {
        return new ApiResponse($data);
    }

    /**
     * @param array $errors All errors encountered while processing the request.
     * @param int $statusCode The error code.
     * @return ApiResponse The response body
     */
    protected function error($errors = [], $statusCode = 500) {
        return new ApiResponse(null, $errors, $statusCode);
    }
}
