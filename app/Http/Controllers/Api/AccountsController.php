<?php

namespace App\Http\Controllers\Api;

use App\ApiResponse;
use App\Http\Controllers\Controller;
use App\Services\AccountsService;
use Illuminate\Support\Facades\Log;

/**
 * @Controller(prefix="api/v1/accounts", middleware="api")
 */
class AccountsController extends Controller
{

    /**
     * @var AccountsService AccountsService instance to be used.
     */
    protected $accountsService;

    /**
     * AccountsController constructor.
     * @param AccountsService $accountsService AutoInjected Service.
     */
    public function __construct(AccountsService $accountsService)
    {
        Log::info("Constructing AccountsController...");
        $this->accountsService = $accountsService;
    }

    /**
     * @GET("/all")
     * @return ApiResponse
     */
    public function getAccounts()
    {
        Log::info("Received request to retrieve accounts...");
        return $this->success($this->accountsService->getAccounts());
    }

    /**
     * @GET("/{accountNumber}")
     * @param $accountNumber string The accountNumber to search for.
     * @return ApiResponse The response returned.
     */
    public function getAccountByAccountNumber($accountNumber)
    {
        Log::info("Received request to retrieve account for account number: " . $accountNumber);
        return $this->success($this->accountsService->getAccount($accountNumber));
    }
}
