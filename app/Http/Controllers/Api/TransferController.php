<?php

namespace App\Http\Controllers\Api;

use App\ApiModels\TransferRequest;
use App\ApiResponse;
use App\Http\Controllers\Controller;
use App\Services\TransferService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use PhpParser\JsonDecoder;
use Symfony\Component\Serializer\Encoder\JsonDecode;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Controller(prefix="api/v1/transfer", middleware="api")
 */
class TransferController extends Controller
{
    protected $transferService;

    public function __construct(TransferService $transferService)
    {
        Log::info("Creating Transfer Controller");
        $this->transferService = $transferService;
    }

    /**
     * @POST("/")
     * @param Request $request
     * @return ApiResponse
     */
    public function sendMoney(Request $request)
    {
        Log::info("Received request to transfer money...");

        $content = $request->getContent();
        Log::info("Request Content: ". $content);

        $encoder = new JsonEncoder(null, new JsonDecode());
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getName();
        });

        $serializer = new Serializer(array($normalizer), array($encoder));
//        var_dump($serializer->serialize($org, 'json'));
//        $serializer = new Serializer();
        /** @var TransferRequest $transferRequest */
        $transferRequest = $serializer->deserialize($content, TransferRequest::class, 'json');
        Log::info("Deserialized amount:".$transferRequest->getAmount());
        /** @var double $balance */
        $balance = $this->transferService->transfer($transferRequest->getFromAccount(), $transferRequest->getToAccount(), $transferRequest->getAmount());
        return $this->success($balance);
    }

}
